// -*- C++ -*-
#pragma once

#include <cstddef>

#include <dsn/log/exports.h>

namespace dsn {
namespace log {
    /// @brief Namespace for version number constants
    namespace version {
        extern const size_t DSN_LOG_EXPORT MAJOR; //!< Major version number
        extern const size_t DSN_LOG_EXPORT MINOR; //!< Minor version number
        extern const size_t DSN_LOG_EXPORT PATCH; //!< Patch level

    } // namespace version
} // namespace log
} // namespace dsn
