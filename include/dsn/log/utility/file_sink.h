// -*- C++ -*-
#pragma once

#include <dsn/log/sink.h>

namespace dsn {
namespace log {
    namespace utility {
        dsn::log::sink make_file_sink(const std::string& filename);
    }
}
}
