// -*- C++ -*-
#pragma once

#include <dsn/log/sink.h>

namespace dsn {
namespace log {
    namespace utility {
        dsn::log::sink make_console_sink();
    } // namespace utility
} // namespace log
} // namespace dsn
