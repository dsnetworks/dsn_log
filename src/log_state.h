// -*- C++ -*-
#pragma once

#include <dsn/log/level.h>

#include <atomic>

namespace dsn {
namespace log {
    /**
     * @brief Logging library implementation details
     *
     * @internal The code in this namespace should not be used in client code and isn't part of the libary's external
     * API.
     */
    namespace detail {
        /**
         * @brief Message processing state for a log level
         *
         * This helper class is used internally to store an atomic boolean value for each available log
         * level that is used to check whether messages for the level shall be processed or dropped.
         *
         * @see dsn::log::level
         * @see dsn::log::is_level_enabled
         * @see dsn::log::set_level_enabled
         *
         * @note This wrapper class is required to be able to store an `std::array` of atomic boolean values
         */
        class log_state {
        public:
            log_state(dsn::log::level log_level, bool enabled = true);

            log_state(const log_state& rhs);
            log_state& operator=(const log_state& rhs);

            log_state(log_state&&) noexcept;
            log_state& operator=(log_state&&) noexcept;

            ~log_state() = default;

            dsn::log::level level() const;
            bool            is_enabled() const;
            void            set_enabled(bool enabled = true);

        private:
            dsn::log::level  m_level;   //!< Wrapped log level
            std::atomic_bool m_enabled; //!< Current state of message processing
        };
    } // namespace detail
} // namespace log
} // namespace dsn
