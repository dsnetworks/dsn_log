// -*- C++ -*-
#pragma once

#include "sink.h"

#include <atomic>
#include <functional>
#include <mutex>
#include <thread>

#include <boost/thread/sync_queue.hpp>

#include <dsn/log/config.h>

#ifdef dsn_log_WITH_gsl
#include <gsl/gsl>
#endif

#include <dsn/log/exports.h>

namespace dsn {
namespace log {
    /**
     * @brief Main logger interface
     *
     * This class manages a variable number of sink instances that can be fed with messages created by
     * its operator() overload.
     *
     * The messages are processed asynchronously by a separate thread so that logging doesn't block for I/O
     * in the main thread.
     */
    class DSN_LOG_EXPORT logger {
    private:
        /// @brief Type alias for message dispatch functors
        using functor_t = std::function<void()>;

        boost::sync_queue<functor_t> m_queue;       //!< Queue for message dispatch functors
        std::thread                  m_thread;      //!< Worker thread to call functors from m_queue
        std::atomic_bool             m_thread_done; //!< done flag for m_thread

        void dispatch_messages();

        std::vector<sink> m_sinks;       //!< Log sinks for this logger
        std::mutex        m_sinks_mutex; //!< Mutex for modifications on m_sinks

    public:
        explicit logger();
        ~logger();

        message operator()(level log_level, const std::string& file, int line);

        void add_sink(const sink& log_sink);
        void remove_sink(const sink& log_sink);
        void clear_sinks();

        /// @brief Type alias for a pointer to a message
#ifdef dsn_log_WITH_gsl
        using message_ptr_t = gsl::not_null<message*>;
#else
        using message_ptr_t = dsn::log::message*;
#endif // !dsn_log_WITH_gsl
        void flush(message_ptr_t message);

        static logger& instance();
    };
} // namespace log
} // namespace dsn
