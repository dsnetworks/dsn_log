#include <dsn/log/config.h>
#include <dsn/log/utility/file_sink.h>

#include <chrono>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <sstream>

#include <boost/filesystem/path.hpp>
#ifndef STDCXX_HAVE_std_quoted
#include <boost/io/detail/quoted_manip.hpp>
#endif

namespace dsn {
namespace log {
    namespace utility {
        namespace {
            /**
             * @brief Callable sink for log files
             *
             * This wraps an std::ofstream in a struct that implements the callable sink interface.
             */
            struct file_sink {
                /**
                 * @brief Create sink for log file
                 * @param filename Name of the target file for this sink
                 *
                 * @todo Read/write sharing on Windows
                 */
                explicit file_sink(const std::string& filename)
                    : file(std::make_shared<std::ofstream>(filename))
                {
                    if (!file->good()) {
                        std::stringstream message;
                        message << "Failed to open log file "
#ifdef STDCXX_HAVE_std_quoted
                                << std::quoted(filename)
#else
                                << boost::io::quoted(filename)
#endif // !dsn_log_HAVE_CXX_STD_QUOTED
                            ;
                        throw std::runtime_error(message.str());
                    }
                }

                void operator()(const dsn::log::message::meta& meta, const std::string& text) const
                {
                    auto              now        = std::chrono::system_clock::now();
                    auto              t          = std::chrono::system_clock::to_time_t(now);
                    auto              local_time = std::localtime(&t);
                    auto              filename   = boost::filesystem::path(meta.file).filename().string();
                    std::stringstream line;

#ifdef STDCXX_HAVE_std_put_time
                    line << std::put_time(local_time, "%F %T");
#else
                    char buffer[128];
                    std::strftime(buffer, sizeof(buffer), "%F %T", local_time);
                    line << buffer;
#endif
                    line << std::setw(7) << std::left << meta.level << " " << filename << ":" << meta.line << ": "
                         << text;

                    (*file) << line.str() << std::endl;
                }

                std::shared_ptr<std::ostream> file;
            };
        } // namespace

        /**
         * @brief Helper function to create a log sink for a text file
         * @param filename Filename to which log messages shall be written
         * @return sink that writes all messages to the given text file
         */
        sink make_file_sink(const std::string& filename)
        {
            return file_sink(filename);
        }
    } // namespace utility
} // namespace log
} // namespace dsn
