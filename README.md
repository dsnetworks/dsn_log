# libdsn_log README
This library provides a lightweight asynchronous logging framework for C++ applications.

## Dependencies
### Required
* [Boost](https://www.boost.org/)
* C++11 capable compiler, e.g.
 * GCC >= 4.8
 * Clang

### Optional
* Guideline Support Library ([GSL](https://github.com/Microsoft/GSL))
* Doxygen


## Building
Use CMake to configure and build the library.

### Build options
* `dsn_log_WITH_TESTS` - Build unit tests
    * `dsn_log_USE_INCLUDED_UNIT_TEST_FRAMEWORK` - Use header-only Boost.Test framework for unit tests
* `dsn_log_WITH_CPACK` - Include CPack to build binary packages
* `dsn_log_WITH_DOXYGEN` - Generate API documentation using Doxygen
* `dsn_log_WITH_gsl` - Use GSL templates for pointer safety (only available if GSL headers have been found in `CMAKE_PREFIX_PATH`)
