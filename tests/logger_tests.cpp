#define BOOST_TEST_MODULE "dsn::log::logger"

#include <dsn/log/config.h>
#include <dsn/log.h>
#include <dsn/log/utility/console_sink.h>

#include <boost/test/unit_test.hpp>
#ifdef dsn_log_USE_INCLUDED_UNIT_TEST_FRAMEWORK
#include <boost/test/included/unit_test_framework.hpp>
#endif

BOOST_AUTO_TEST_CASE(default_construction) { dsn::log::logger test_logger; }

BOOST_AUTO_TEST_CASE(singleton_instance) { auto& logger = dsn::log::logger::instance(); }

namespace {
dsn::log::sink g_sink{ dsn::log::utility::make_console_sink() };
}

BOOST_AUTO_TEST_CASE(add_sink)
{
    auto& logger = dsn::log::logger::instance();
    logger.add_sink(g_sink);
};

#if !(__GNUC__ && ((__GNUC__ * 10000 + __GNUC_MINOR__ * 100) < 50100))
BOOST_AUTO_TEST_CASE(message_creation_operator)
{
    auto& logger = dsn::log::logger::instance();
    {
        auto msg = logger(dsn::log::level::Info, __FILE__, __LINE__);
        msg << "This is a test";
    }
}
#endif // GCC < 5.1

BOOST_AUTO_TEST_CASE(remove_sink)
{
    auto& logger = dsn::log::logger::instance();
    logger.remove_sink(g_sink);
    logger.add_sink(g_sink);
}

BOOST_AUTO_TEST_CASE(clear_sinks)
{
    auto& logger = dsn::log::logger::instance();
    logger.clear_sinks();
    logger.add_sink(g_sink);
}

BOOST_AUTO_TEST_CASE(log_macros)
{
    dsn_log(Trace) << "first trace test";
    dsn_log_Trace << "second trace test";

    dsn_log_Debug << "debug test";
    dsn_log_Info << "info test";
    dsn_log_Warning << "warning test";
    dsn_log_Error << "error test";
    dsn_log_Fatal << "fatal test";
}
