#define BOOST_TEST_MODULE "dsn::log::sink"

#include <dsn/log/config.h>
#include <dsn/log/sink.h>
#include <dsn/log/utility/console_sink.h>
#include <dsn/log/utility/file_sink.h>

#include <boost/filesystem.hpp>

#include <boost/test/unit_test.hpp>
#ifdef dsn_log_USE_INCLUDED_UNIT_TEST_FRAMEWORK
#include <boost/test/included/unit_test_framework.hpp>
#endif

BOOST_AUTO_TEST_CASE(construct_sink_from_lambda)
{
    bool message_written{ false };

    dsn::log::sink sink(
        [&message_written](const dsn::log::message::meta&, const std::string&) { message_written = true; });

    sink.write({ dsn::log::level::Info, __FILE__, __LINE__ }, "test message");
    BOOST_REQUIRE(message_written == true);
}

namespace {
class test_functor {
private:
    bool& m_triggered;

public:
    test_functor(bool& out_variable)
        : m_triggered(out_variable)
    {
    }

    void operator()(const dsn::log::message::meta&, const std::string&) { m_triggered = true; }
};
}

BOOST_AUTO_TEST_CASE(construct_sink_from_function_object)
{
    bool         triggered{ false };
    test_functor functor(triggered);
    BOOST_REQUIRE(triggered == false);

    dsn::log::sink sink(functor);
    sink.write({ dsn::log::level::Info, __FILE__, __LINE__ }, "test message");
    BOOST_REQUIRE(triggered == true);
}

BOOST_AUTO_TEST_CASE(make_console_sink)
{
    auto sink = dsn::log::utility::make_console_sink();
    sink.write({ dsn::log::level::Info, __FILE__, __LINE__ }, "test message (make_console_sink)");
}

BOOST_AUTO_TEST_CASE(make_file_sink)
{
    auto filename = boost::filesystem::unique_path("%%%%%%.log");
    {
        auto sink = dsn::log::utility::make_file_sink(filename.string());
        sink.write({ dsn::log::level::Info, __FILE__, __LINE__ }, "test message (make_console_sink)");
    }

    boost::filesystem::remove(filename);
}
