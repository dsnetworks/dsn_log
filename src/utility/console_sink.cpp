#include <dsn/log/config.h>
#include <dsn/log/utility/console_sink.h>

#include <chrono>
#include <ctime>
#include <iomanip>
#include <iostream>

#include <boost/filesystem/path.hpp>

/**
 * @brief Helper function to create a log sink on `std::clog`
 * @return sink that writes all messages to `std::clog`
 */
dsn::log::sink dsn::log::utility::make_console_sink()
{
    return [](const dsn::log::message::meta& meta, const std::string& text) {
        if (!dsn::log::is_level_enabled(meta.level)) {
            return;
        }

        auto              now        = std::chrono::system_clock::now();
        auto              t          = std::chrono::system_clock::to_time_t(now);
        auto              local_time = std::localtime(&t);
        auto              filename   = boost::filesystem::path(meta.file).filename().string();
        std::stringstream line;
#ifdef STDCXX_HAVE_std_put_time
        line << std::put_time(local_time, "%F %T");
#else
        char buffer[128];
        std::strftime(buffer, sizeof(buffer), "%F %T", local_time);
        line << buffer;
#endif
        line << " " << std::setw(7) << std::left << meta.level << " " << filename << ":" << meta.line << ": " << text
             << "\n";

        std::clog << line.str();
    };
}
