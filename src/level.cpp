#include <dsn/log/level.h>

#include "log_state.h"

#include <array>
#include <iostream>
#include <utility>

#include <boost/algorithm/string/case_conv.hpp>

namespace dsn {
namespace log {
    namespace {
        using name_pair_t = std::pair<dsn::log::level, std::string>;
        const std::array<name_pair_t, 6> g_level_names{
            std::make_pair(level::Trace, "trace"), std::make_pair(level::Debug, "debug"),
            std::make_pair(level::Info, "info"),   std::make_pair(level::Warning, "warning"),
            std::make_pair(level::Error, "error"), std::make_pair(level::Fatal, "fatal"),
        };

        std::array<detail::log_state, 6> g_log_states{
            level::Trace, level::Debug, level::Info, level::Warning, level::Error, level::Fatal,
        };
    } // namespace

    /**
     * @brief Set minimum level
     *
     * Activates all log levels with a priority equal to or higher than the given level.
     *
     * @param log_level Minimum priority for log messages to be processed
     */
    void set_minimum_level(level log_level)
    {
        using U        = typename std::underlying_type<level>::type;
        auto requested = static_cast<U>(log_level);

        for (auto& state : g_log_states) {
            state.set_enabled(static_cast<U>(state.level()) >= requested);
        }
    }

    /// @brief Get current minimum log level
    level minimum_level()
    {
        for (const auto& state : g_log_states) {
            if (state.is_enabled()) {
                return state.level();
            }
        }

        throw std::runtime_error("Unable to determine minimum log level!");
    }

} // namespace log
} // namespace dsn

/**
 * @brief Write log level to stream
 * @param stream Output stream to which level shall be written
 * @param log_level level enumerator that shall be written to stream
 * @return stream after writing level
 */
std::ostream& dsn::log::operator<<(std::ostream& stream, const dsn::log::level& log_level)
{
    auto it = std::find_if(g_level_names.begin(), g_level_names.end(),
                           [log_level](const name_pair_t& pair) { return pair.first == log_level; });
    if (it != g_level_names.end()) {
        return stream << (*it).second;
    }

    return stream << "unknown";
}

/**
 * @brief Parse log level from stream
 *
 * Reads one word from `stream` and tries to parse it into a log level enumerator value.
 *
 * @param stream Input stream from which a log level shall be read
 * @param log_level Target variable where the parsed log level shall be stored
 * @return stream after reading a value into level
 */
std::istream& dsn::log::operator>>(std::istream& stream, dsn::log::level& log_level)
{
    std::string level_string;
    {
        std::istream::sentry sentry(stream, false);
        if (!sentry) {
            return stream;
        }

        stream >> level_string;
        boost::algorithm::to_lower(level_string);
    }

    auto it = std::find_if(g_level_names.begin(), g_level_names.end(),
                           [&level_string](const name_pair_t& pair) { return pair.second == level_string; });
    if (it != g_level_names.end()) {
        log_level = (*it).first;
    } else {
        stream.setstate(std::ios::failbit);
    }

    return stream;
}

/**
 * @brief Check if messages for a level are enabled
 * @param log_level Log level that shall be checked
 * @return Boolean that indicates whether messages for `log_level` are currently enabled
 */
bool dsn::log::is_level_enabled(dsn::log::level log_level)
{
    auto it = std::find_if(g_log_states.begin(), g_log_states.end(),
                           [log_level](const detail::log_state& state) { return state.level() == log_level; });
    if (it != g_log_states.end()) {
        auto& state = (*it);
        return state.is_enabled();
    }

    return false;
}

/**
 * @brief Enable/disable message processing for a level
 * @param log_level Log level that shall be modified
 * @param enabled Boolean that indicates whether messages for `log_level` shall be processed
 */
void dsn::log::set_level_enabled(dsn::log::level log_level, bool enabled)
{
    auto it = std::find_if(g_log_states.begin(), g_log_states.end(),
                           [log_level](const detail::log_state& state) { return state.level() == log_level; });
    if (it != g_log_states.end()) {
        auto& state = (*it);
        state.set_enabled(enabled);
    }
}
