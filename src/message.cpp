#include <dsn/log/logger.h>
#include <dsn/log/message.h>

/**
 * @brief Apply ostream modifier
 * @return Reference to this instance after applying `fn` to the internal buffer
 */
dsn::log::message& dsn::log::message::operator<<(std::ostream& (*fn)(std::ostream&))
{
    fn(m_buffer);

    return *this;
}

/**
 * @brief Create new message instance
 * @param owner Pointer to the owning logger instance
 * @param level Log level of the new message
 * @param file Source file name where the message was generated
 * @param line Line number in `file`
 */
dsn::log::message::message(dsn::log::logger* owner, dsn::log::level level, const std::string& file, int line)
    : m_owner(owner)
    , m_meta{ level, file, line }
{
}

#if !(__GNUC__ && ((__GNUC__ * 10000 + __GNUC_MINOR__ * 100) < 50100))
/**
 * @brief Move-construct from another message
 * @param rhs Oher message instance that shall be moved into this one
 */
dsn::log::message::message(dsn::log::message&& rhs) noexcept
    : m_owner(nullptr)
    , m_buffer(std::move(rhs.m_buffer))
    , m_meta(std::move(rhs.m_meta))
{
    std::swap(m_owner, rhs.m_owner);
}

/**
 * @brief Move-assignment operator
 * @param rhs Other message instance that shall be moved into this one
 * @return Reference to this instance after moving from `rhs`
 */
dsn::log::message& dsn::log::message::operator=(dsn::log::message&& rhs) noexcept
{
    m_owner  = nullptr;
    m_buffer = std::move(rhs.m_buffer);
    m_meta   = std::move(rhs.m_meta);
    std::swap(m_owner, rhs.m_owner);

    return *this;
}
#endif

/**
 * @brief Destroy message instance
 */
dsn::log::message::~message()
{
    if (m_owner != nullptr) {
        if (m_buffer) {
            m_owner->flush(this);
        }
    }
}
