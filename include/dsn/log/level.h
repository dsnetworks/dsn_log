// -*- C++ -*-
#pragma once

#include <dsn/log/exports.h>

#include <iosfwd>

namespace dsn {
namespace log {
    /// @brief Enumeration for a log message's priority
    enum class level {
        Trace,
        Debug,
        Info,
        Warning,
        Error,
        Fatal,
    };

    std::ostream& DSN_LOG_EXPORT operator<<(std::ostream& stream, const level& log_level);
    std::istream& DSN_LOG_EXPORT operator>>(std::istream& stream, level& log_level);

    bool is_level_enabled(level log_level);
    void set_level_enabled(level log_level, bool enabled = true);

    void DSN_LOG_EXPORT set_minimum_level(level log_level);
    level DSN_LOG_EXPORT minimum_level();

} // namespace log
} // namespace dsn
