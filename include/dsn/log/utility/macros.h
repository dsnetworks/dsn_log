// -*- C++ -*-
#pragma once

#include <dsn/log/logger.h>

#define dsn_log(log_level) (dsn::log::logger::instance()(dsn::log::level::log_level, __FILE__, __LINE__))

#define dsn_log_Trace dsn_log(Trace)
#define dsn_log_Debug dsn_log(Debug)
#define dsn_log_Info dsn_log(Info)
#define dsn_log_Warning dsn_log(Warning)
#define dsn_log_Error dsn_log(Error)
#define dsn_log_Fatal dsn_log(Fatal)
