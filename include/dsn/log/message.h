// -*- C++ -*-
#pragma once

#include "level.h"

#include <sstream>

#include <dsn/log/exports.h>

namespace dsn {
namespace log {
    class logger;

    /**
     * @brief Log message
     *
     * This class wraps the meta data (source file, line number, priority) and text of a single log message.
     * The message provides an iostream-like interface so that arbitrary values can be appended to the message text.
     *
     * @note It shouldn't be required to manually create instances of this class when using logger's operator()
     *
     * @see logger
     */
    class DSN_LOG_EXPORT message {
    private:
        friend logger; //!< logger needs to access our internal buffer

        logger* m_owner; //!< Pointer to owning logger instance

        message(logger* owner, level level, const std::string& file, int line);

        message(const message&) = delete;
        message& operator=(const message&) = delete;

#if !(__GNUC__ && ((__GNUC__ * 10000 + __GNUC_MINOR__ * 100) < 50100))
        message(message&& rhs) noexcept;
        message& operator=(message&& rhs) noexcept;
#endif
        std::ostringstream m_buffer; //!< Buffer for message text

    public:
        ~message();

        template <typename T> message& operator<<(const T& value);
        message&                       operator<<(std::ostream& (*fn)(std::ostream&));

        /// @brief Metadata for a log message
        struct DSN_LOG_EXPORT meta {
            dsn::log::level level; //!< Priority of this message
            std::string     file;  //!< Source file where the message was generated
            int             line;  //!< Line numbe rwhere the message was generated
        };

    private:
        meta m_meta; //!< Metadata of this message
    };

    /**
     * @brief Append value to message buffer
     *
     * @tparam T Type of the value that shall be appended
     *
     * @param value Value that shall be appended to the message's buffer
     *
     * @return Reference to this message instance after appending `value` to the internal buffer
     */
    template <typename T> message& message::operator<<(const T& value)
    {
        m_buffer << value;
        return *this;
    }
} // namespace log
} // namespace dsn
