#include <dsn/log/logger.h>

/// @brief Entry point for message dispatch thread
void dsn::log::logger::dispatch_messages()
{
    while (!m_thread_done.load()) {
        functor_t functor;
        m_queue.wait_pull(functor);
        functor();
    }
}

/**
 * @brief Create new logger instance
 *
 * Launches a background thread to process incoming messages from `m_queue`
 */
dsn::log::logger::logger()
    : m_thread_done(false)
    , m_thread(&logger::dispatch_messages, this)
{
}

/**
 * @brief Destroy logger instance
 *
 * Stops the worker thread for dispatching messages and waits for it to flush any previously queued messages.
 */
dsn::log::logger::~logger()
{
    m_queue.push([this] { m_thread_done.store(true); });
    m_thread.join();
}

/**
 * @brief Create new message instance
 *
 * Creates a message instance with the owner set to this logger.
 *
 * @param log_level Priority of the newly created message
 * @param file Source file that triggered the message
 * @param line Line number in `file`
 * @return message instance with the data from the operator's input parameters
 *
 * @see message
 */
dsn::log::message dsn::log::logger::operator()(dsn::log::level log_level, const std::string& file, int line)
{
    return { this, log_level, file, line };
}

/**
 * @brief Add sink to logger
 *
 * Adds a sink to this logger so that it receives messages for processing.
 *
 * @param log_sink sink that shall be added to this logger
 */
void dsn::log::logger::add_sink(const dsn::log::sink& log_sink)
{
    std::lock_guard<std::mutex> guard(m_sinks_mutex);
    auto                        it = std::find(m_sinks.begin(), m_sinks.end(), log_sink);
    if (it == m_sinks.end()) {
        m_sinks.push_back(std::move(log_sink));
    }
}

/**
 * @brief Remove sink from logger
 *
 * Removes the given sink from the logger so that it doesn't receive any future messages.
 *
 * @param log_sink
 */
void dsn::log::logger::remove_sink(const dsn::log::sink& log_sink)
{
    std::lock_guard<std::mutex> guard(m_sinks_mutex);
    auto                        it = std::find(m_sinks.begin(), m_sinks.end(), log_sink);
    if (it != m_sinks.end()) {
        m_sinks.erase(it);
    }
}

/**
 * @brief Remove all log sinks
 */
void dsn::log::logger::clear_sinks()
{
    std::lock_guard<std::mutex> guard(m_sinks_mutex);
    m_sinks.clear();
}

/**
 * @brief Flush message to sinks
 *
 * Queues the message from the given pointer in `m_queue` so that it gets written to all currently
 * registered log sinks.
 *
 * @param message Pointer to the message that shall be flushed
 *
 * @note This is called from message's destructor so a message is automatically flushed when it goes out of scope
 *
 * @see message
 */
void dsn::log::logger::flush(dsn::log::logger::message_ptr_t message)
{
    decltype(m_sinks) sinks;
    auto              meta = message->m_meta;
    auto              text = message->m_buffer.str();
    {
        std::lock_guard<std::mutex> guard(m_sinks_mutex);
        sinks.reserve(m_sinks.size());
        sinks = m_sinks;
    }

    m_queue.push([=] {
        for (auto& sink : sinks) {
            sink.write(meta, text);
        }
    });
}

/**
 * @brief Access global logger singleton
 * @return Reference to a statically created logger instance
 */
dsn::log::logger& dsn::log::logger::instance()
{
    static logger s_instance;
    return s_instance;
}
