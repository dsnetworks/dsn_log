// -*- C++ -*-
#pragma once

#include "log/level.h"
#include "log/logger.h"
#include "log/message.h"
#include "log/version.h"

#include "log/utility/macros.h"

namespace dsn {
/**
 * @brief \#das-system networks logging library
 *
 * This namespace contains all classes related to our custom asynchronous logging library.
 */
namespace log {
    /**
     * @brief Utility classes for logging library
     *
     * This namespace contains various helper functions and classes that make using the logging
     * library more convenient.
     */
    namespace utility {
    } // namespace utility
} // namespace log
} // namespace dsn
