#define BOOST_TEST_MODULE "dsn::log::level"

#include <dsn/log/config.h>
#include <dsn/log/level.h>

#include <iostream>
#include <map>

#include <boost/test/unit_test.hpp>
#ifdef dsn_log_USE_INCLUDED_UNIT_TEST_FRAMEWORK
#include <boost/test/included/unit_test_framework.hpp>
#endif

BOOST_AUTO_TEST_CASE(iostream_output)
{
    using dsn::log::level;
    const std::map<level, std::string> levels_to_check{
        { level::Trace, "trace" },     { level::Debug, "debug" }, { level::Info, "info" },
        { level::Warning, "warning" }, { level::Error, "error" }, { level::Fatal, "fatal" },
    };
    for (const auto& pair : levels_to_check) {
        std::stringstream stream;
        stream << pair.first;
        BOOST_REQUIRE(stream.str() == pair.second);
    }
}

BOOST_AUTO_TEST_CASE(iostream_input)
{
    using dsn::log::level;
    const std::map<level, std::vector<std::string>> levels_to_check{
        std::make_pair(level::Trace, std::vector<std::string>{ "trace", "TRACE", "TrAcE" }),
        std::make_pair(level::Debug, std::vector<std::string>{ "debug", "Debug" }),
        std::make_pair(level::Info, std::vector<std::string>{ "info" }),
        std::make_pair(level::Warning, std::vector<std::string>{ "warning" }),
        std::make_pair(level::Error, std::vector<std::string>{ "error" }),
        std::make_pair(level::Fatal, std::vector<std::string>{ "fatal" })
    };

    for (const auto& pair : levels_to_check) {
        for (const auto& string : pair.second) {
            std::stringstream stream;
            dsn::log::level   log_level{ level::Trace };

            stream << string;
            stream >> log_level;
            BOOST_REQUIRE(log_level == pair.first);
        }
    }
}

BOOST_AUTO_TEST_CASE(is_level_enabled)
{
    using dsn::log::level;
    for (auto lvl : { level::Trace, level::Debug, level::Info, level::Warning, level::Error, level::Fatal }) {
        BOOST_REQUIRE(dsn::log::is_level_enabled(lvl) == true);
    }
}

BOOST_AUTO_TEST_CASE(set_level_enabled)
{
    using dsn::log::level;
    for (auto lvl : { level::Trace, level::Debug, level::Info, level::Warning, level::Error, level::Fatal }) {
        BOOST_REQUIRE(dsn::log::is_level_enabled(lvl) == true);

        dsn::log::set_level_enabled(lvl, false);
        BOOST_REQUIRE(dsn::log::is_level_enabled(lvl) == false);

        dsn::log::set_level_enabled(lvl);
        BOOST_REQUIRE(dsn::log::is_level_enabled(lvl) == true);
    }
}

BOOST_AUTO_TEST_CASE(minimum_level_api)
{
    auto min_level = dsn::log::minimum_level();
    BOOST_REQUIRE_EQUAL(min_level, dsn::log::level::Trace);

    dsn::log::set_minimum_level(dsn::log::level::Info);
    BOOST_REQUIRE_EQUAL(dsn::log::minimum_level(), dsn::log::level::Info);

    dsn::log::set_minimum_level(min_level);
    BOOST_REQUIRE_EQUAL(dsn::log::minimum_level(), min_level);
}
