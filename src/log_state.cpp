#include "log_state.h"

#include <utility>

/**
 * @brief Create new state object
 *
 * @param log_level Log level for which the state shall be created
 * @param enabled Initial state of `log_level` message processing
 */
dsn::log::detail::log_state::log_state(dsn::log::level log_level, bool enabled)
    : m_level(log_level)
    , m_enabled(enabled)
{
}

/**
 * @brief Copy-construct from another log_state
 * @param rhs log_state that shall be copied
 */
dsn::log::detail::log_state::log_state(const dsn::log::detail::log_state& rhs)
    : m_level(rhs.m_level)
    , m_enabled(rhs.m_enabled.load())
{
}

/**
 * @brief Move-construct from another log_state
 * @param rhs log_state that shall be moved into this one
 */
dsn::log::detail::log_state::log_state(dsn::log::detail::log_state&& rhs) noexcept
    : m_level(std::move(rhs.m_level))
    , m_enabled(rhs.m_enabled.load())
{
}

/**
 * @brief Move-assign from another log_state
 * @param rhs log_state that shall be moved into this one
 */
dsn::log::detail::log_state& dsn::log::detail::log_state::operator=(dsn::log::detail::log_state&& rhs) noexcept
{
    m_level = rhs.m_level;
    m_enabled.store(rhs.m_enabled.load());

    return *this;
}

/**
 * @brief Copy-assign from another log_state
 * @param rhs log_state that shall be copied
 * @return Reference to this log_state after copying `rhs`
 */
dsn::log::detail::log_state& dsn::log::detail::log_state::operator=(const dsn::log::detail::log_state& rhs)
{
    m_level = rhs.m_level;
    m_enabled.store(rhs.m_enabled.load());

    return *this;
}

/**
 * @brief Accessor for state
 * @return Priority that is enabled/disabled by this state object
 */
dsn::log::level dsn::log::detail::log_state::level() const { return m_level; }

/**
 * @brief Accessor for enabled/disabled flag
 * @return true if messages for the wrapped level are processed, otherwise false
 */
bool dsn::log::detail::log_state::is_enabled() const { return m_enabled.load(); }

/**
 * @brief Set enabled/disabled flag
 * @param enabled Boolean value that indicates whether messages shall be processed
 */
void dsn::log::detail::log_state::set_enabled(bool enabled) { m_enabled.store(enabled); }
