// -*- C++ -*-
#pragma once

#include "message.h"

#include <memory>

#include <dsn/log/config.h>

#ifdef dsn_log_WITH_gsl
#include <gsl/gsl>
#endif

#include <dsn/log/exports.h>

namespace dsn {
namespace log {
    /**
     * @brief Interface for log sinks
     *
     * This class provides an interface so that any callable (lambda, function, etc.) can be used as log sink
     * as long as it provides a matching function call operator.
     */
    class DSN_LOG_EXPORT sink {
    private:
        /// @brief Abstract base class for sink implementations
        struct DSN_LOG_EXPORT concept {
            virtual ~concept() = default;

            /// @brief Type alias for an owning pointer to a concept
#ifdef dsn_log_WITH_gsl
            using concept_ptr_t = gsl::owner<concept*>;
#else
            using concept_ptr_t = concept*;
#endif // !dsn_log_WITH_gsl

            /**
             * @brief Create a clone of this concept
             * @return Pointer to a copy of this concept
             */
            virtual concept_ptr_t clone() const = 0;

            /**
             * @brief Write message to sink
             * @param meta Metadata of the message that shall be written
             * @param message Message text that shall be written
             */
            virtual void write(const message::meta& meta, const std::string& message) = 0;
        };

        /// @brief Template for sink implementations based on callables
        template <typename T> struct model : concept {
            /**
             * @brief Create new sink wrapper
             * @param impl Callable that shall be used as a log sink
             */
            model(T impl)
                : m_impl(std::move(impl)){};

            /// @brief Callable implementation for this sink
            T m_impl;

            // concept interface
        public:
            /**
             * @brief Create copy of this implementation
             * @return Owning pointer containing a copy of this sink implementation
             */
            virtual concept::concept_ptr_t clone() const override { return new model<T>(m_impl); }

            /**
             * @brief Write log message to sink
             * @param meta Metadata for the log message that shall be written
             * @param message Message text as string
             */
            virtual void write(const message::meta& meta, const std::string& message) override
            {
                m_impl(meta, message);
            }
        };

        /// @brief Wrapped callable sink implementation
        std::unique_ptr<concept> m_wrapper;

    public:
        /**
         * @brief Construct sink from callable
         *
         * This constructs a sink from any callable (lambda, function object, function pointer) that provides an
         * `operator()(const message::meta&, const std::string&)` operator.
         *
         * @tparam T Type of the callable that shall be used as a log sink
         *
         * @param impl Callable that shall be wrapped into a log sink
         */
        template <typename T>
        sink(T impl)
            : m_wrapper(new model<T>(std::move(impl)))
        {
        }

        sink(const sink& rhs);
        sink& operator=(const sink& rhs);

        sink(sink&& rhs) noexcept;
        sink& operator=(sink&& rhs) noexcept;

        void write(const message::meta& meta, const std::string& message) const;

        bool operator==(const sink& rhs) const;
        bool operator!=(const sink& rhs) const;
    };
} // namespace log
} // namespace dsn
