#include <dsn/log/sink.h>

/**
 * @brief Copy-construct from another sink
 * @param rhs Reference to the sink that shall be copied
 */
dsn::log::sink::sink(const dsn::log::sink& rhs)
    : m_wrapper(rhs.m_wrapper->clone())
{
}

/**
 * @brief Copy-assign from another sink
 * @param rhs Reference to the sink that shall be copied
 * @return Reference to this object after cloning `rhs`
 */
dsn::log::sink& dsn::log::sink::operator=(const dsn::log::sink& rhs)
{
    m_wrapper.reset(rhs.m_wrapper->clone());

    return *this;
}

/**
 * @brief Move-construct from another sink
 * @param rhs Sink that shall be moved into this one
 */
dsn::log::sink::sink(dsn::log::sink&& rhs) noexcept
    : m_wrapper(std::move(rhs.m_wrapper))
{
}

/**
 * @brief Move-assign from another sink
 * @param rhs Sink that shall be moved into this one
 * @return Reference to this instance after moving from `rhs`
 */
dsn::log::sink& dsn::log::sink::operator=(dsn::log::sink&& rhs) noexcept
{
    m_wrapper = std::move(rhs.m_wrapper);

    return *this;
}

/**
 * @brief Write message to sink
 * @param meta Metadata for the log message that shall be written
 * @param message Message text as string
 */
void dsn::log::sink::write(const dsn::log::message::meta& meta, const std::string& message) const
{
    m_wrapper->write(meta, message);
}

/**
 * @brief Comparison operator (equality)
 * @param rhs sink to which this one shall be compared
 * @return `true` if the two sink instances use the same wrapper, otherwise `false`
 */
bool dsn::log::sink::operator==(const dsn::log::sink& rhs) const { return m_wrapper.get() == rhs.m_wrapper.get(); }

/**
 * @brief Comparison operator (inequality)
 * @param rhs sink to which this one shall be compared
 * @return `true` if the two sink instances use different wrappers, otherwise `false`
 */
bool dsn::log::sink::operator!=(const dsn::log::sink& rhs) const { return m_wrapper.get() != rhs.m_wrapper.get(); }
